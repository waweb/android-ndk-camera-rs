//! # Android NDK
//!
//! Bindings to the Android NDKCamera.
#![warn(missing_debug_implementations)]

pub mod camera_capture_session;
pub mod camera_device;
pub mod camera_manager;
pub mod camera_metadata;
pub mod camera_window_type;
pub mod capture_request;