# Rust Bindings for Android NDKCamera

`android-ndk-camera` is a safe wrapper to the [Android NDKCamera API](https://developer.android.com/ndk/reference/group/camera).

This project is in the inception phase, please refer to active branches for ongoing development work.